const express = require("express");
const app = express();
const port = 5001;
const fs = require("fs");

app.set("view engine", "ejs");
app.use(express.static("public"));

const data = fs.readFileSync("./user.json");
const jsonData = JSON.parse(data);
const user = jsonData["user"];
const pwd = jsonData["pwd"];

app.get("/", (req, res) => {
  res.render("index", { user, pwd });
});

app.get("/games", (req, res) => {
  res.render("games", { user, pwd });
});

app.listen(port, () => {
  console.log("port running atport number :" + port);
  console.log("user :" + user);
  console.log("pwd :" + pwd);
});
